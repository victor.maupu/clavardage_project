package modele;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Date;


public class User implements Serializable{
	private String pseudo;
	public static transient ArrayList<User> listeUsers = new ArrayList<User>();
	private String adresseIP;
	private long timeOut;
	
	public User(String pseudo) {
        this.pseudo = pseudo;
        User.listeUsers.add(this);
        this.adresseIP = null;
        Date date = new Date();
        this.timeOut = date.getTime();
    }
	
	public User(String pseudo, String adresseIP) {
        this.pseudo = pseudo;
        User.listeUsers.add(this);
        this.adresseIP = adresseIP;
        Date date = new Date();
        this.timeOut = date.getTime();
    }
	
	public User(String pseudo, String adresseIP, int index) {
        this.pseudo = pseudo;
        User.listeUsers.add(index, this);
        this.adresseIP = adresseIP;
        Date date = new Date();
        this.timeOut = date.getTime();
    }
	
	public String getPseudo() {
		return this.pseudo;
	}
	public long getTimeOut() {
		return this.timeOut;
	}
	public void setTimeOut() {
		try { 
	        Date date = new Date();
	        this.timeOut = date.getTime();
		} catch (NullPointerException e) {
    		e.printStackTrace();
		}
	}
	public String getAdresseIP()
	{
		return this.adresseIP;
	}
	
	public void setAdresseIP(String ip)
	{
		this.adresseIP = ip;
	}
	
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	
	public static User getUserByPseudo(String pseudo)
	{
		for (User u : listeUsers)
		{
			if (u.getPseudo().equals(pseudo))
			{
				return u;
			}
		}
		return null;
	}

	/*
	 * Retourne si le pseudo est dans la liste
	 * 
	 */
	public static String PseudoIsInListUser(String pseudo) {
		for (User u : User.listeUsers) {
			try {
				if(u.pseudo.equals(pseudo)) {
					return u.adresseIP;
				}
			}catch (NullPointerException e) {
				
			}
		}
		return null;
	}
	
	public static User getUserByIP(String ip)
	{
		for (User u : listeUsers)
		{
			if (u.getAdresseIP().equals(ip))
			{
				return u;
			}
		}
		return null;
	}

		/*
	 * Retourne l'objet user de qui on est
	 * 
	 */
	public static User QuiSuisJe() {
		if(User.listeUsers.isEmpty()) {
			return null;
		} else {
			return User.listeUsers.get(0);			
		}
	}
	
	public static void changeMyName(String pseudo) {
		if(!User.listeUsers.isEmpty()) {
			User.listeUsers.get(0).pseudo = pseudo;			
		}
	}
	
	public static String listUsertoString() {
		String l = "{\"Users\":[";
		for (int i = 0;i<User.listeUsers.size();i++) {
		    l = l + "{\"pseudo\":\"" + listeUsers.get(i).pseudo + "\",\"ip\":\"" + listeUsers.get(i).adresseIP+ "\"}";
			if(i!=User.listeUsers.size()-1) {
				l = l + ",";
			}
		}
		return l + "]}";
	}
}
