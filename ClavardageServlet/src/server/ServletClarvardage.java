package server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modele.User;

/**
 * Servlet implementation class ServletClarvardage
 */
public class ServletClarvardage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletClarvardage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		TimeOutUser t = new TimeOutUser();
		t.start();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		try {
			String type = request.getParameter("type");
			if(type.equals("IMHERE")) {
				try {
					String pseudo = request.getParameter("pseudo");
					String ipUser = User.PseudoIsInListUser(pseudo);
		            if(ipUser == null) {
		            	new User(pseudo, request.getRemoteHost());
		            	String listeuser = User.listUsertoString();
		            	pw.write(listeuser);
		            } else {
		            	pw.write("[ERREURPSEUDO]");
		            }
				} catch (NullPointerException e) {
					pw.write("No pseudo imhere");
					e.printStackTrace();
				}
			} else if(type.equals("WHOISHERE")) {
				try {
					String pseudo = request.getParameter("pseudo");
					String ipUser = User.PseudoIsInListUser(pseudo);
					if(ipUser == null && pseudo!=null) {
		            	new User(pseudo, request.getRemoteHost());
		            	String listeuser = User.listUsertoString();
		            	pw.write(listeuser);
		            } else {
		            	User.getUserByPseudo(pseudo).setTimeOut();
		            	String listeuser = User.listUsertoString();
		            	pw.write(listeuser);
		            }
				} catch (NullPointerException e) {
					String listeuser = User.listUsertoString();
	            	pw.write(listeuser);
				}
			} else if(type.equals("ILEAVE")) {
				User userLeaving = User.getUserByIP(request.getRemoteHost());
				pw.write("SeeYouSoon " + request.getRemoteHost() );
				User.listeUsers.remove(userLeaving);
			} else if(type.equals("ICHANGEPSEUDO")) {
				try {
					String pseudo = request.getParameter("pseudo");
					String ipUser = User.PseudoIsInListUser(pseudo);
		            if(ipUser == null) {
						User userLchanging = User.getUserByIP(request.getRemoteHost());
		            	User.listeUsers.remove(userLchanging);
		            	new User(pseudo, request.getRemoteHost());
		            	String listeuser = User.listUsertoString();
		            	pw.write(listeuser);
		            } else {
		            	pw.write("[ERREURPSEUDO]");
		            }
				} catch (NullPointerException e) {
					pw.write("No pseudo");
				}
			}
		} catch (NullPointerException e) {
			pw.write("No type"); 
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
