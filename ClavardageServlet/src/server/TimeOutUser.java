package server;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import modele.User;

public class TimeOutUser extends Thread {
	public void run() {
		Timer t = new Timer();
		t.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					for (int i = User.listeUsers.size() - 1 ; i >= 0 ; i--) {
						long userTime = User.listeUsers.get(i).getTimeOut();
						Date nowD = new Date();
						long now = nowD.getTime();
						if(now-userTime > 8001) {
							User.listeUsers.remove(User.listeUsers.get(i));
						}
					}
				}catch (NullPointerException e) {

				}

			}
		}, 0, 4000);
	}
}
