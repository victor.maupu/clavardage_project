# clavardage_Project

## Utilisation de l'application

Il faut lancer l'application grâce au fichier .jar (qui se trouve à la racine du projet) et à la commande suivante :
```
java -jar clavardage_project_v1.5.jar
```
Pour utiliser l'application en mode serveur, il faut tout d'abord lancer ce serveur. Pour cela suivez le tutos de la vidéo [TutoUtilisationServlet](Videos/TutoUtilisationServlet.mp4).

Vous pouvez également suivre les tutos [TutoModeLocal](Videos/TutoModeLocal.mp4) et [TutoModeExterne](Videos/TutoModeExterne.mp4) pour apprendre à utiliser l'application.


## PROTOCOLES DE CONNEXION ET D'ACTUALISATION DE L'APPLICATION

## Protocole de connexion en mode local :

###	L'utilisateur se connecte :

- abonnement à l'adresse de multicast 230.0.0.0
- envoie d'une trame multicast de type `[IMHERE]`
- Si réception d'une trame TCP de type `[ERREURPSEUDO]`, reset du processus de connexion
- Si réception d'une trame TCP de type `[METOOIMHERE]`, ajout du nouvel utilisateur grâce à son ip et à son pseudo présent dans le contenu du message
- Si aucune trame `[ERREURPSEUDO]` reçue, passage à l'état connecté

###	L'utilisateur est connecté :

- Réception d'une trame multicast de type `[IMHERE]`, si pseudo non utilisé, envoie d'une trame TCP de type `[METOOIMHERE]` et actualisation des utilisateurs connectés, sinon envoie d'une trame TCP de type `[ERREURPSEUDO]`
- Réception d'une trame multicast de type `[ILEAVE]`, suppression de l'utilisateur emmeteur de la liste des utilisateurs connectés
- Réception d'une trame multicast de type `[ICHANGEPSEUDO]`, si pseudo non utilisé, actualisation du nom de l'utilisateur emmeteur sinon envoie d'une trame TCP de type `[ERREURCHGPSEUDO]`

###	L'utilisateur change de pseudo :

- Envoie d'une trame multicast de type `[ICHANGEPSEUDO]`
- Si réception d'une trame TCP de type `[ERREURCHGPSEUDO]`, echec du changement de pseudo, sinon changement du pseudo

###	L'utilisateur se déconnecte :

- Envoie d'une trame multicast de type `[ILEAVE]`

## Protocole de connexion en mode externe :

###	L'utilisateur se connecte :

- Requête http GET au serveur de type :`http://@ipServeur:8080/ClavardageServlet/ServletClarvardage?type=IMHERE&pseudo=pseudo`
- Si réponse = `ERREURPSEUDO`, reset du processus de connexion
- Sinon réception de la liste des utilisateurs connectés sous un tableau au format JSON

###	L'utilisateur est connecté :

- Envoie continue de requête http GET au serveur de type :`http://@ipServeur:8080/ClavardageServlet/ServletClarvardage?type=WHOISHERE&pseudo=pseudo`
- Actualise la liste des utilisateurs connectés grâce au retour sous format JSON

###	L'utilisateur change de pseudo :

- Requête http GET au serveur de type : `http://@ipServeur:8080/ClavardageServlet/ServletClarvardage?type=ICHANGEPSEUDO&pseudo=newPseudo`
- Si réponse =`ERREURCHGPSEUDO`, échec du changement de pseudo
- Sinon changement ok

### L'utilisateur se déconnecte :

- Requête http GET au serveur de type : `http://@ipServeur:8080/ClavardageServlet/ServletClarvardage?type=ILEAVE&pseudo=pseudo`

###	Le serveur de présence :

- Reçoit une requête http GET de type :
	- `IMHERE` -> vérification du pseudo et renvoie de `ERREURPSEUDO` si pseudo interdit ou renvoie d'un tableau au format JSON des utilisateurs connectés si pseudo autorisé
	- `WHOISHERE` -> renvoie d'un tableau au format JSON des utilisateurs connectés + actualisation du Timer de l'utilisateur émetteur ou création de l'utilisateur si il est inconnu
	- `ILEAVE` -> actualisation de la liste des utilisateurs connectés, renvoie SeeYouSoon
	- `ICHANGEPSEUDO` -> vérification du pseudo et renvoie de `ERREURCHGPSEUDO` si pseudo interdit ou renvoie d'un tableau au format JSON des utilisateurs connectés si pseudo autorisé
- Continuellement vérifie le timer de chaque utilisateur connecté, si celui-ci ne s'est pas manifesté depuis trop longtemps il est considéré comme non connecté

## BASE DE DONNÉES

Si il y a une erreur lors de l'initialisation de la base de données, alors un popup s'affiche pour en informer l'utilisateur.
En conséquence, les messages ne seront pas enregistrés et l'historique ne sera pas accessible. Cependant, cela ne perturbera pas le bon fonctionnement de l'application.
