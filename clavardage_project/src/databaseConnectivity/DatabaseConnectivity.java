package databaseConnectivity;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import Exceptions.DatabaseException;
import modele.*;

public class DatabaseConnectivity {

    static Connection conn = null;
    static DatabaseMetaData meta = null;
    static Statement stmt = null;
    static PreparedStatement pstmt = null;
    static boolean useDB = true;

    public static void createDatabase() {
	String url = "jdbc:sqlite:database.db";
	try {
	    DatabaseConnectivity.conn = DriverManager.getConnection(url);
	    if (conn != null) {
		DatabaseConnectivity.meta = conn.getMetaData();
		DatabaseConnectivity.stmt = conn.createStatement();

		String req = "CREATE TABLE IF NOT EXISTS USER (id integer PRIMARY KEY,ip text);";

		stmt.execute(req);

		req = "CREATE TABLE IF NOT EXISTS MESSAGE(id integer PRIMARY KEY, date LONG, contenu text, "
			+ "idEmetteur integer, idRecepteur integer,FOREIGN KEY(idEmetteur) REFERENCES USER(id),FOREIGN KEY(idRecepteur) REFERENCES USER(id));";

		stmt.execute(req);

	    }
	} catch (SQLException e) {
	    useDB = false;
	    try {
		throw new DatabaseException();
	    } catch (DatabaseException e1) {
	    }

	}
    }

    public static void addUser(User u) {
	if (useDB) {
	    if (DatabaseConnectivity.getIdUser(u.getAdresseIP()) == -1) {
		try {
		    DatabaseConnectivity.pstmt = conn.prepareStatement("INSERT INTO USER (ip) VALUES (?)");
		    DatabaseConnectivity.pstmt.setString(1, u.getAdresseIP());
		    DatabaseConnectivity.pstmt.executeUpdate();
		} catch (SQLException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	    }
	}
    }

    public static void addMessage(Message m) {
	if (useDB) {
	    Date d = m.getDate();
	    String c = m.getContenu();
	    int idE = getIdUser(m.getEmetteur().getAdresseIP());
	    if (idE == -1) {
		DatabaseConnectivity.addUser(m.getEmetteur());
		idE = DatabaseConnectivity.getIdUser(m.getEmetteur().getAdresseIP());
	    }

	    int idR = getIdUser(m.getRecepteur().getAdresseIP());
	    if (idR == -1) {
		DatabaseConnectivity.addUser(m.getRecepteur());
		idE = DatabaseConnectivity.getIdUser(m.getRecepteur().getAdresseIP());
	    }

	    try {
		DatabaseConnectivity.pstmt = conn.prepareStatement(
			"INSERT INTO MESSAGE (date, contenu, idEmetteur,idRecepteur) VALUES (?,?,?,?)");
		DatabaseConnectivity.pstmt.setLong(1, d.getTime());
		DatabaseConnectivity.pstmt.setString(2, c);
		DatabaseConnectivity.pstmt.setInt(3, idE);
		DatabaseConnectivity.pstmt.setInt(4, idR);

		pstmt.executeUpdate();
	    } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}

    }

    public static ArrayList<Message> getMessages(User u1, User u2) {
	ArrayList<Message> listMess = new ArrayList<Message>();
	if (useDB) {
	    int idE = DatabaseConnectivity.getIdUser(u1.getAdresseIP());
	    int idR = DatabaseConnectivity.getIdUser(u2.getAdresseIP());

	    String sql = "SELECT id,date,contenu,idEmetteur,idRecepteur FROM MESSAGE WHERE idEmetteur=" + idE
		    + " and idRecepteur=" + idR + ";";

	    try {
		DatabaseConnectivity.stmt = DatabaseConnectivity.conn.createStatement();

		ResultSet rs = DatabaseConnectivity.stmt.executeQuery(sql);

		while (rs.next()) {
		    listMess.add(new Message(new Date(rs.getLong("date")), rs.getString("contenu"), u1, u2));
		}

	    } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    return listMess;
	}
	return listMess;
    }

    public static ArrayList<Message> getOrderedConversation(User u1, User u2) {
	ArrayList<Message> result = new ArrayList<Message>();
	result.addAll(DatabaseConnectivity.getMessages(u1, u2));
	result.addAll(DatabaseConnectivity.getMessages(u2, u1));
	Collections.sort(result);
	return result;
    }

    public static int getIdUser(String adresseIP) {
	if (useDB) {
	    String sql = "SELECT id, ip FROM USER WHERE ip=\"" + adresseIP + "\";";
	    int id = -1;
	    try {
		DatabaseConnectivity.stmt = DatabaseConnectivity.conn.createStatement();

		ResultSet rs = DatabaseConnectivity.stmt.executeQuery(sql);

		while (rs.next()) {
		    id = rs.getInt("id");
		}

	    } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    return id;
	} else {
	    return -1;
	}
    }

    public static User getUserByIP(String adresseIP) {
	if (useDB) {
	    String sql = "SELECT id, ip FROM USER WHERE ip=\"" + adresseIP + "\";";
	    String ip = "";
	    try {
		DatabaseConnectivity.stmt = DatabaseConnectivity.conn.createStatement();

		ResultSet rs = DatabaseConnectivity.stmt.executeQuery(sql);

		while (rs.next()) {
		    ip = rs.getString("ip");
		}

	    } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    User u = User.getUserByIP(ip);
	    return u;
	} else {
	    return null;
	}
    }

}
