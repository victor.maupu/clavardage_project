package modele;

public class Mode {
    private static Boolean local;
    private static Boolean externe;
    private static String ipServeur;

    public Mode(Boolean externe, String ipServeur) {
	Mode.externe = true;
	Mode.local = false;
	Mode.ipServeur = ipServeur;

    }

    public Mode(Boolean local) {
	Mode.externe = false;
	Mode.local = true;
	Mode.ipServeur = null;

    }

    static public Boolean isLocalMode() {
	if (Mode.local) {
	    return true;
	} else {
	    return false;
	}
    }

    static public Boolean isExternMode() {
	if (Mode.externe) {
	    return true;
	} else {
	    return false;
	}
    }

    static public String getIpServeur() {
	return Mode.ipServeur;
    }
}
