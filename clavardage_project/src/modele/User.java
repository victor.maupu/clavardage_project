package modele;

import java.io.Serializable;
import java.util.ArrayList;

import databaseConnectivity.DatabaseConnectivity;

public class User implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -7980706332304564882L;
    private String pseudo;
    public static transient ArrayList<User> listeUsers = new ArrayList<User>();
    private String adresseIP;

    public User(String pseudo) {
	this.pseudo = pseudo;
	User.listeUsers.add(this);
	this.adresseIP = null;

    }

    public User(String pseudo, String adresseIP) {
	this.pseudo = pseudo;
	User.listeUsers.add(this);
	this.adresseIP = adresseIP;
	DatabaseConnectivity.addUser(this);

    }

    public User(String pseudo, String adresseIP, int index) {
	this.pseudo = pseudo;
	User.listeUsers.add(index, this);
	this.adresseIP = adresseIP;
	DatabaseConnectivity.addUser(this);
    }

    public String getPseudo() {
	return this.pseudo;
    }

    public String getAdresseIP() {
	return this.adresseIP;
    }

    public void setAdresseIP(String ip) {
	this.adresseIP = ip;
    }

    public void setPseudo(String pseudo) {
	this.pseudo = pseudo;
    }

    public static User getUserByPseudo(String pseudo) {
	for (User u : listeUsers) {
	    if (u.getPseudo().equals(pseudo)) {
		return u;
	    }
	}
	return null;
    }

    /*
     * Retourne si le pseudo est dans la liste
     * 
     */
    public static String PseudoIsInListUser(String pseudo) {
	for (User u : User.listeUsers) {
	    if (u.pseudo.equals(pseudo)) {
		return u.adresseIP;
	    }
	}
	return null;
    }

    public static User getUserByIP(String ip) {
	for (User u : listeUsers) {
	    if (u.getAdresseIP().equals(ip)) {
		if (u != User.QuiSuisJe()) {
		    return u;
		}
	    }
	}
	return null;
    }

    /*
     * Retourne l'objet user de qui on est
     * 
     */
    public static User QuiSuisJe() {
	if (User.listeUsers.isEmpty()) {
	    return null;
	} else {
	    return User.listeUsers.get(0);
	}
    }

    public static void changeMyName(String pseudo) {
	if (!User.listeUsers.isEmpty()) {
	    User.listeUsers.get(0).pseudo = pseudo;
	}
    }

    public synchronized static void actualiseList(ArrayList<String> listeUsersPresent) {
	for (int i = User.listeUsers.size() - 1; i >= 0; i--) {
	    if (User.listeUsers.get(i) != User.QuiSuisJe()
		    && !listeUsersPresent.contains(User.listeUsers.get(i).getPseudo())) {
		User.listeUsers.remove(User.listeUsers.get(i));
	    }
	}

    }
}
