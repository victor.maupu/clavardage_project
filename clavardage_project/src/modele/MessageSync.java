package modele;

import java.io.Serializable;

public class MessageSync implements Serializable {
    private static final long serialVersionUID = 1L;
    String type;
    String contenu;
    User contact;

    public MessageSync(String contenu, User emmeteur, String type) {
	this.type = type;
	this.contenu = contenu;
	this.contact = emmeteur;
    }

    public String getType() {
	return this.type;
    }

    public String getContenu() {
	return this.contenu;
    }

    public User getEmmeteur() {
	return this.contact;
    }

}
