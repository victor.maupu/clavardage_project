package modele;

import java.net.Socket;
import java.util.ArrayList;
import controller.ConversationController;
import unicastConnectivity.UnicastConnectivity;
import views.ConversationView;

public class Conversation {
    User user1;
    User user2;
    UnicastConnectivity unicastCo;
    ArrayList<Message> lmessages = new ArrayList<Message>();
    ConversationView cv;

    public Conversation(User user, ConversationController cv) {
	this.user1 = User.QuiSuisJe();
	this.user2 = user;
	unicastCo = new UnicastConnectivity(user2, cv);
    }

    public Conversation(Socket sock, ConversationController cv, User u) {
	user1 = User.QuiSuisJe();
	user2 = u;
	try {
	    unicastCo = new UnicastConnectivity(sock, cv, user2);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }

    public ArrayList<User> getUsers() {
	ArrayList<User> list = new ArrayList<User>();
	list.add(this.user1);
	list.add(this.user2);
	return list;
    }

    public User getUser2() {
	return this.user2;
    }

    public void addMessage(Message message) {
	this.lmessages.add(message);

    }

    public ArrayList<Message> getMessages() {
	return this.lmessages;
    }

    public synchronized void receiveMessage(Message m) {
	this.lmessages.add(m);
    }

    public UnicastConnectivity getUnicastConnectivity() {
	return this.unicastCo;
    }

}
