package modele;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable, Comparable<Message> {
    private static final long serialVersionUID = 1L;
    Date date;
    String contenu;
    User emetteur;
    User recepteur;

    public Message(Date date, String contenu, User emeteur, User recepteur) {
	this.date = date;
	this.contenu = contenu;
	this.emetteur = emeteur;
	this.recepteur = recepteur;
    }

    public Date getDate() {
	return this.date;
    }

    public void setEmetteur(User u) {
	this.emetteur = u;
    }

    public String getContenu() {
	return this.contenu;
    }

    public User getEmetteur() {
	return this.emetteur;
    }

    public String getDateFormat() {
	String res = "";
	String[] d = this.date.toString().split(" ");
	String[] d1 = d[3].split(":");
	String d2 = d1[0] + ":" + d1[1];
	res += d[2] + " " + d[1] + " " + d2 + " : ";
	res += this.emetteur.getPseudo();
	res += " : ";
	res += this.contenu;
	return res;
    }

    public String toString() {
	String res = "";
	String[] d = this.date.toString().split(" ");
	res += d[3];
	res += this.emetteur.getPseudo();
	res += " : ";
	res += this.contenu;
	return res;
    }

    public void setRecepteur(User u) {
	this.recepteur = u;

    }

    public User getRecepteur() {
	return this.recepteur;
    }

    @Override
    public int compareTo(Message o) {
	return (int) (this.getDate().getTime() - o.getDate().getTime());
    }
}
