package views;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import actionListener.ConnexionViewListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ButtonGroup;

import java.awt.Color;
import java.awt.Component;
import javax.swing.SpringLayout;
import java.awt.Label;
import java.awt.Font;
import javax.swing.UIManager;
import java.awt.SystemColor;
import javax.swing.JRadioButton;

public class ConnexionView extends JFrame {
    /**
     * 
     */
    private static final long serialVersionUID = -5602466728187682569L;
    public JTextField txtPseudo;
    public Label pseudo_false;
    public Label nopseudo;
    public JRadioButton rdbtnLocal;
    public JRadioButton rdbtnExterne;
    public JTextField IpServeur;

    /**
     * Create the frame.
     */
    public ConnexionView() {
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setBounds(100, 100, 450, 300);
	JPanel panel = new JPanel();
	panel.setBackground(SystemColor.controlHighlight);
	panel.setForeground(UIManager.getColor("FormattedTextField.selectionForeground"));
	getContentPane().add(panel, BorderLayout.CENTER);

	JButton btnNewButton = new JButton("Connexion");
	btnNewButton.setFont(new Font("DejaVu Sans Mono", Font.BOLD, 12));
	btnNewButton.setBackground(UIManager.getColor("ComboBox.selectionBackground"));
	btnNewButton.setAlignmentX(0.5f);
	btnNewButton.addActionListener(new ConnexionViewListener(this));
	SpringLayout sl_panel = new SpringLayout();
	panel.setLayout(sl_panel);

	txtPseudo = new JTextField();
	txtPseudo.setText("Pseudo");
	sl_panel.putConstraint(SpringLayout.NORTH, btnNewButton, 0, SpringLayout.NORTH, txtPseudo);
	sl_panel.putConstraint(SpringLayout.WEST, btnNewButton, 33, SpringLayout.EAST, txtPseudo);
	sl_panel.putConstraint(SpringLayout.SOUTH, txtPseudo, -135, SpringLayout.SOUTH, panel);
	sl_panel.putConstraint(SpringLayout.EAST, txtPseudo, 224, SpringLayout.WEST, panel);
	sl_panel.putConstraint(SpringLayout.NORTH, txtPseudo, 111, SpringLayout.NORTH, panel);
	sl_panel.putConstraint(SpringLayout.WEST, txtPseudo, 35, SpringLayout.WEST, panel);
	txtPseudo.setAlignmentX(Component.LEFT_ALIGNMENT);
	txtPseudo.setForeground(Color.DARK_GRAY);
	txtPseudo.addActionListener(new ConnexionViewListener(this));
	panel.add(txtPseudo);
	txtPseudo.setColumns(8);
	panel.add(btnNewButton);

	Label label = new Label("Entrez votre pseudo");
	label.setFont(new Font("DejaVu Sans Mono", Font.PLAIN, 12));
	sl_panel.putConstraint(SpringLayout.WEST, label, 0, SpringLayout.WEST, txtPseudo);
	sl_panel.putConstraint(SpringLayout.SOUTH, label, -14, SpringLayout.NORTH, txtPseudo);
	panel.add(label);

	pseudo_false = new Label("Pseudo incorrect !");
	sl_panel.putConstraint(SpringLayout.WEST, pseudo_false, 0, SpringLayout.WEST, txtPseudo);
	sl_panel.putConstraint(SpringLayout.SOUTH, pseudo_false, -97, SpringLayout.SOUTH, panel);
	pseudo_false.setForeground(Color.RED);
	pseudo_false.setVisible(false);
	panel.add(pseudo_false);

	nopseudo = new Label("Veuillez rentrer un pseudo");
	sl_panel.putConstraint(SpringLayout.NORTH, nopseudo, 8, SpringLayout.SOUTH, pseudo_false);
	sl_panel.putConstraint(SpringLayout.WEST, nopseudo, 0, SpringLayout.WEST, txtPseudo);
	nopseudo.setForeground(Color.RED);
	nopseudo.setVisible(false);
	panel.add(nopseudo);

	rdbtnLocal = new JRadioButton("Local");
	rdbtnLocal.setSelected(true);
	sl_panel.putConstraint(SpringLayout.NORTH, rdbtnLocal, 6, SpringLayout.SOUTH, nopseudo);
	sl_panel.putConstraint(SpringLayout.EAST, rdbtnLocal, -10, SpringLayout.EAST, nopseudo);
	panel.add(rdbtnLocal);

	rdbtnExterne = new JRadioButton("Externe");
	sl_panel.putConstraint(SpringLayout.NORTH, rdbtnExterne, 6, SpringLayout.SOUTH, nopseudo);
	sl_panel.putConstraint(SpringLayout.WEST, rdbtnExterne, 6, SpringLayout.EAST, rdbtnLocal);
	panel.add(rdbtnExterne);

	// Group the radio buttons.
	ButtonGroup group = new ButtonGroup();
	group.add(rdbtnLocal);
	group.add(rdbtnExterne);

	JLabel lblIPServeur = new JLabel("IP serveur de présence :");
	sl_panel.putConstraint(SpringLayout.NORTH, lblIPServeur, 6, SpringLayout.SOUTH, rdbtnLocal);
	sl_panel.putConstraint(SpringLayout.WEST, lblIPServeur, 74, SpringLayout.WEST, panel);
	panel.add(lblIPServeur);

	IpServeur = new JTextField();
	sl_panel.putConstraint(SpringLayout.NORTH, IpServeur, -2, SpringLayout.NORTH, lblIPServeur);
	sl_panel.putConstraint(SpringLayout.WEST, IpServeur, 7, SpringLayout.EAST, lblIPServeur);
	IpServeur.addActionListener(new ConnexionViewListener(this));
	panel.add(IpServeur);
	IpServeur.setColumns(10);
    }
}
