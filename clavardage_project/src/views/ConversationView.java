package views;

import javax.swing.JFrame;
import java.awt.Dimension;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import actionListener.SendFileListener;
import actionListener.sendListener;
import controller.ConversationController;
import modele.Message;
import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.WindowEvent;
import javax.swing.UIManager;

public class ConversationView {

    private JFrame frame;
    private DefaultListModel<String> arl;
    public ConversationController c;
    public JTextArea textPane;
    JList<String> list;
    private final JButton btnNewButton_1 = new JButton("Envoyer fichier");

    /**
     * Create the application.
     */
    public ConversationView(ConversationController c) {
	this.c = c;
	initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
	frame = new JFrame("Conversation avec " + c.getUsers().get(1).getPseudo());
	frame.setVisible(true);
	frame.setBounds(100, 100, 450, 300);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 3 };
	gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3 };
	gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE, 0.0, 0.0 };
	gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
	frame.getContentPane().setLayout(gridBagLayout);
	arl = new DefaultListModel<>();
	list = new JList<String>(arl);
	// list.setPreferredSize(new Dimension(200, 200));
	GridBagConstraints gbc_list = new GridBagConstraints();
	gbc_list.gridheight = 6;
	gbc_list.gridwidth = 10;
	gbc_list.insets = new Insets(0, 0, 5, 0);
	gbc_list.fill = GridBagConstraints.BOTH;
	gbc_list.gridx = 0;
	gbc_list.gridy = 0;
	JScrollPane scrollPane = new JScrollPane(list);

	frame.getContentPane().add(scrollPane, gbc_list);

	JButton btnNewButton = new JButton("Envoyer");
	btnNewButton.setFont(UIManager.getFont("ComboBox.font"));
	btnNewButton.setBackground(UIManager.getColor("Button.darkShadow"));
	btnNewButton.addActionListener(new sendListener(this));
	GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
	gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
	gbc_btnNewButton.gridx = 9;
	gbc_btnNewButton.gridy = 7;
	frame.getContentPane().add(btnNewButton, gbc_btnNewButton);
	GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
	gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 0);
	gbc_btnNewButton_1.gridx = 9;
	gbc_btnNewButton_1.gridy = 8;
	btnNewButton_1.addActionListener(new SendFileListener(this.c.c.getUser2()));
	frame.getContentPane().add(btnNewButton_1, gbc_btnNewButton_1);
	textPane = new JTextArea(50, 50);
	GridBagConstraints gbc_textPane = new GridBagConstraints();
	gbc_textPane.gridheight = 3;
	gbc_textPane.gridwidth = 8;
	gbc_textPane.insets = new Insets(0, 0, 0, 5);
	gbc_textPane.fill = GridBagConstraints.BOTH;
	gbc_textPane.gridx = 0;
	gbc_textPane.gridy = 7;
	textPane.setLineWrap(true);
	textPane.setWrapStyleWord(true);
	// textPane.addActionListener(new sendListener(this));
	textPane.setPreferredSize(new Dimension(200, 200));
	JScrollPane scrollPane2 = new JScrollPane(textPane);
	frame.getContentPane().add(scrollPane2, gbc_textPane);
	frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	frame.addWindowListener(new java.awt.event.WindowAdapter() {
	    public void windowClosing(WindowEvent winEvt) {
		c.closeSocket();
	    }
	});
	// frame.addWindowListener(new MyWindowListener());
    }

    public void addMessageRecu(Message m) {
	arl.addElement(m.getDateFormat());
    }

    public void addMessageEnvoye(Message m) {
	arl.addElement(m.getDateFormat());
    }

    public void close() {
	arl.addElement(c.getUsers().get(1).getPseudo() + " a quitté la conversation");

    }

    public void reactivate() {
	frame.setVisible(true);
	arl.addElement(c.getUsers().get(1).getPseudo() + " a rejoint la conversation");
    }

}
