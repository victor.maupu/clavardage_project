package views;

import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;

import java.awt.event.WindowEvent;
import actionListener.MainListViewListener;
import actionListener.MainViewListener;

import javax.swing.SpringLayout;

import modele.Mode;
import modele.User;
import multicastConnectivity.MulticastSender;
import multicastConnectivity.MulticastServerRequest;
import multicastConnectivity.ServerRequestWhoishere;
import unicastConnectivity.UnicastAcceptConnectivity;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;

import org.json.JSONException;

public class MainView {

    public JFrame frame;
    private static DefaultListModel<String> arl = new DefaultListModel<>();
    private static JList<String> list;
    // private JTextField newPseudo;
    private JButton btnChangerPseudo;
    private JLabel name;
    private JTextField newPseudo;
    private JButton btnValiderChangement;
    private JLabel lblPseudoDjPris;

    /**
     * Launch the application.
     *//*
        * public static void main(String[] args) { EventQueue.invokeLater(new
        * Runnable() { public void run() { try {btnChangerPseudo MainView window = new
        * MainView(); window.frame.setVisible(true); } catch (Exception e) {
        * e.printStackTrace(); } } }); }
        */
    /**
     * Create the application.
     */
    public MainView() {
	frame = new JFrame();
	frame.setBounds(100, 100, 450, 300);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	SpringLayout springLayout = new SpringLayout();
	frame.getContentPane().setLayout(springLayout);

	JLabel lblContentDeVous = new JLabel("Content de vous revoir ");
	springLayout.putConstraint(SpringLayout.NORTH, lblContentDeVous, 20, SpringLayout.NORTH,
		frame.getContentPane());
	springLayout.putConstraint(SpringLayout.WEST, lblContentDeVous, 24, SpringLayout.WEST, frame.getContentPane());
	frame.getContentPane().add(lblContentDeVous);

	if (User.QuiSuisJe() != null) {
	    name = new JLabel(User.QuiSuisJe().getPseudo());
	} else {
	    name = new JLabel("");
	}
	springLayout.putConstraint(SpringLayout.EAST, name, 205, SpringLayout.EAST, lblContentDeVous);
	name.setFont(new Font("Dialog", Font.BOLD, 16));
	springLayout.putConstraint(SpringLayout.NORTH, name, -7, SpringLayout.NORTH, lblContentDeVous);
	springLayout.putConstraint(SpringLayout.WEST, name, 18, SpringLayout.EAST, lblContentDeVous);
	springLayout.putConstraint(SpringLayout.SOUTH, name, 22, SpringLayout.NORTH, lblContentDeVous);
	frame.getContentPane().add(name);

	JButton btnDeconnexion = new JButton("Deconnexion");
	btnDeconnexion.setFont(new Font("Liberation Sans", Font.BOLD, 12));
	btnDeconnexion.setForeground(UIManager.getColor("ScrollPane.foreground"));
	btnDeconnexion.setBackground(UIManager.getColor("TextArea.selectionBackground"));
	btnDeconnexion.addActionListener(new MainViewListener(this));
	springLayout.putConstraint(SpringLayout.SOUTH, btnDeconnexion, -10, SpringLayout.SOUTH, frame.getContentPane());
	springLayout.putConstraint(SpringLayout.EAST, btnDeconnexion, -10, SpringLayout.EAST, frame.getContentPane());
	frame.getContentPane().add(btnDeconnexion);

	MainView.arl = new DefaultListModel<>();
	list = new JList<String>(MainView.arl);
	springLayout.putConstraint(SpringLayout.NORTH, list, 27, SpringLayout.SOUTH, lblContentDeVous);
	springLayout.putConstraint(SpringLayout.WEST, list, 40, SpringLayout.WEST, frame.getContentPane());
	springLayout.putConstraint(SpringLayout.SOUTH, list, 209, SpringLayout.SOUTH, lblContentDeVous);
	springLayout.putConstraint(SpringLayout.EAST, list, 205, SpringLayout.WEST, frame.getContentPane());
	list.addMouseListener(new MainListViewListener(this));
	frame.getContentPane().add(list);

	btnChangerPseudo = new JButton("Changer de pseudo");
	springLayout.putConstraint(SpringLayout.EAST, btnChangerPseudo, 0, SpringLayout.EAST, btnDeconnexion);
	btnChangerPseudo.addActionListener(new MainViewListener(this));
	frame.getContentPane().add(btnChangerPseudo);

	newPseudo = new JTextField();
	springLayout.putConstraint(SpringLayout.SOUTH, btnChangerPseudo, -11, SpringLayout.NORTH, newPseudo);
	springLayout.putConstraint(SpringLayout.EAST, newPseudo, -10, SpringLayout.EAST, frame.getContentPane());
	frame.getContentPane().add(newPseudo);
	newPseudo.setColumns(10);

	btnValiderChangement = new JButton("Valider changement");
	springLayout.putConstraint(SpringLayout.NORTH, btnValiderChangement, 160, SpringLayout.NORTH,
		frame.getContentPane());
	springLayout.putConstraint(SpringLayout.SOUTH, newPseudo, -1, SpringLayout.NORTH, btnValiderChangement);
	springLayout.putConstraint(SpringLayout.EAST, btnValiderChangement, 0, SpringLayout.EAST, btnDeconnexion);
	btnValiderChangement.addActionListener(new MainViewListener(this));
	frame.getContentPane().add(btnValiderChangement);

	lblPseudoDjPris = new JLabel("Pseudo déjà pris !");
	lblPseudoDjPris.setForeground(Color.RED);
	lblPseudoDjPris.setVisible(false);
	springLayout.putConstraint(SpringLayout.SOUTH, lblPseudoDjPris, -12, SpringLayout.NORTH, btnChangerPseudo);
	springLayout.putConstraint(SpringLayout.EAST, lblPseudoDjPris, -35, SpringLayout.EAST, frame.getContentPane());
	frame.getContentPane().add(lblPseudoDjPris);

	ArrayList<User> listeUser = User.listeUsers;
	for (User i : listeUser) {
	    if (i != User.QuiSuisJe()) {
		arl.addElement(i.getPseudo());
	    }
	}
	changePseudoVisible(false);

	/* === A VOIR SI ON LAISSE ça LA */
	Thread t1 = new Thread(new UnicastAcceptConnectivity());
	t1.start();

	name.setVisible(true);
	this.frame.setVisible(true);
	frame.addWindowListener(new java.awt.event.WindowAdapter() {
	    public void windowClosing(WindowEvent winEvt) {
		if (Mode.isLocalMode()) {
		    MulticastSender t2 = new MulticastSender();
		    t2.multicast("", "[ILEAVE]");
		    System.exit(0);
		} else {
		    ServerRequestWhoishere.stopRequestNow();
		    MulticastServerRequest t = new MulticastServerRequest();
		    try {
			t.request("", "ILEAVE");
		    } catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		    }
		    System.exit(0);
		}
	    }
	});
	frame.addKeyListener(null);
    }

    public void setNewPseudo() {
	this.newPseudo.setText(User.QuiSuisJe().getPseudo());
    }

    public String getNewPseudo() {
	return this.newPseudo.getText();
    }

    public void setName(String name) {
	this.name.setText(name);
    }

    public void setVisiblePseudoDjPris(boolean b) {
	this.lblPseudoDjPris.setVisible(b);
    }

    public void setVisible(boolean b) {
	this.frame.setVisible(b);
    }

    public void changePseudoVisible(boolean b) {
	this.btnValiderChangement.setVisible(b);
	this.btnChangerPseudo.setVisible(!b);
	this.newPseudo.setVisible(b);
    }

    public static DefaultListModel<String> getUserList() {
	return arl;
    }

    public static void addUserList(User e) {
	arl.addElement(e.getPseudo());
    }

    public static void delUserList(User e) {
	arl.removeElement(e.getPseudo());
    }

    public static void deleteUserList(User e) {
	arl.removeElement(e.getPseudo());
    }

    public static boolean ContainUserList(User e) {
	return arl.contains(e.getPseudo());
    }

    public static void actualiseList() {
	ArrayList<User> listeUser = User.listeUsers;
	for (User i : listeUser) {
	    if (i != User.QuiSuisJe() && !ContainUserList(i)) {
		arl.addElement(i.getPseudo());
	    }
	}
	for (int i = arl.getSize() - 1; i >= 0; i--) {
	    String pseudo = arl.getElementAt(i);
	    if (User.getUserByPseudo(pseudo) == null) {
		arl.removeElementAt(i);
	    }
	}

    }

    public static Object getSelectedValue() {
	return MainView.list.getSelectedValue();
    }
}
