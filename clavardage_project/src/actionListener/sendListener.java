package actionListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import views.ConversationView;

public class sendListener implements ActionListener {

    ConversationView cv;

    public sendListener(ConversationView cv) {
	this.cv = cv;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
	String contenu = this.cv.textPane.getText();
	if (!contenu.equals("")) {
	    this.cv.c.sendMessage(contenu);
	}
    }

}
