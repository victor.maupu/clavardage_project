package actionListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import modele.User;
import unicastConnectivity.UnicastFileSender;

public class SendFileListener implements ActionListener {

    User user;;

    public SendFileListener(User u) {
	this.user = u;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
	try {
	    JFileChooser choix = new JFileChooser();
	    choix.showOpenDialog(null);
	    if (choix.getSelectedFile().length() > 160000000) {
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame, "Le fichier dépasse 20Mo,\nimpossible de l'envoyer.");
		frame = null;
	    } else {
		Thread t = new Thread(new UnicastFileSender(this.user, choix.getSelectedFile()));
		t.start();
	    }
	} catch (Exception e) {

	}
    }

}
