package actionListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.json.JSONException;

import modele.Mode;
import modele.User;
import multicastConnectivity.MulticastSender;
import multicastConnectivity.MulticastServerRequest;
import multicastConnectivity.ServerRequestWhoishere;
import views.MainView;

public class MainViewListener implements ActionListener {

    static public MainView mv;
    static private String lastpseudo;

    public MainViewListener(MainView mv) {
	MainViewListener.mv = mv;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

	if (e.getActionCommand() == "Deconnexion") {
	    if (Mode.isLocalMode()) {
		MulticastSender t2 = new MulticastSender();
		t2.multicast("", "[ILEAVE]");
		MainViewListener.mv.setVisible(false);
		System.exit(0);
	    } else {
		ServerRequestWhoishere.stopRequestNow();
		MulticastServerRequest t = new MulticastServerRequest();
		try {
		    t.request("", "ILEAVE");
		} catch (JSONException e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
		MainViewListener.mv.setVisible(false);
		System.exit(0);
	    }

	} else if (e.getActionCommand() == "Changer de pseudo") {
	    mv.changePseudoVisible(true);
	    mv.setNewPseudo();
	} else if (e.getActionCommand() == "Valider changement") {
	    String pseudo = mv.getNewPseudo();
	    if (pseudo != "") {
		mv.setVisiblePseudoDjPris(false);
		if (User.PseudoIsInListUser(pseudo) == null) {
		    lastpseudo = User.QuiSuisJe().getPseudo();
		    mv.changePseudoVisible(false);
		    mv.setName(pseudo);
		    User.changeMyName(pseudo);
		    if (Mode.isLocalMode()) {
			MulticastSender t = new MulticastSender();
			t.multicast("", "[ICHANGEPSEUDO]");
		    } else {
			MulticastServerRequest t = new MulticastServerRequest();
			try {
			    t.request("", "ICHANGEPSEUDO");
			} catch (JSONException e1) {
			    // TODO Auto-generated catch block
			    e1.printStackTrace();
			}
		    }
		} else {
		    mv.changePseudoVisible(false);
		    mv.setVisiblePseudoDjPris(true);
		}

	    }
	}

    }

    public static String getLastPseudo() {
	return lastpseudo;
    }

    public static void actualiseList() {

    }
}
