package actionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import controller.ConversationController;
import modele.User;
import views.MainView;

public class MainListViewListener implements MouseListener {

    static public MainView mv;

    public MainListViewListener(MainView mv) {
	MainViewListener.mv = mv;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
	if (e.getClickCount() == 2) {
	    User useropen = User.getUserByPseudo(MainView.getSelectedValue().toString());
	    @SuppressWarnings("unused")
	    ConversationController c = new ConversationController(useropen);
	}
    }

    @Override
    public void mousePressed(MouseEvent e) {
	// TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
	// TODO Auto-generated method stub

    }

    @Override
    public void mouseEntered(MouseEvent e) {
	// TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
	// TODO Auto-generated method stub

    }

}
