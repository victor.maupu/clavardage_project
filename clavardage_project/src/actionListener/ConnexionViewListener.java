package actionListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.ConnexionController;
import controller.ConnexionControllerServer;
import modele.Mode;
import views.ConnexionView;

public class ConnexionViewListener implements ActionListener {

    static public ConnexionView cv;

    public ConnexionViewListener(ConnexionView cv) {
	ConnexionViewListener.cv = cv;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
	String contenu = ConnexionViewListener.cv.txtPseudo.getText();
	if (contenu.length() != 0) {
	    ConnexionViewListener.cv.setVisible(false);
	    ConnexionViewListener.cv.nopseudo.setVisible(false);
	    if (ConnexionViewListener.cv.rdbtnLocal.isSelected()) {
		new Mode(true);
		new ConnexionController(contenu);
	    } else {
		new Mode(true, ConnexionViewListener.cv.IpServeur.getText());
		new ConnexionControllerServer(contenu);
	    }
	} else {
	    ConnexionViewListener.cv.nopseudo.setVisible(true);
	}
    }

}
