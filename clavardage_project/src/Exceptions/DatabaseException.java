package Exceptions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DatabaseException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -7855732935737495374L;

    public DatabaseException() {
	JFrame frame = new JFrame();
	JOptionPane.showMessageDialog(frame,
		"Un problème est survenu avec la base de données. Les conversations ne sont plus enregistrés. \nSi le problème"
			+ " persiste, contactez votre administrateur réseaux.");
    }
}
