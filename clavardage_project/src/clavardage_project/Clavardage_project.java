package clavardage_project;

import java.awt.EventQueue;

import databaseConnectivity.DatabaseConnectivity;
import unicastConnectivity.UnicastFileAcceptConnectivity;
import views.ConnexionView;

public class Clavardage_project {

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
	DatabaseConnectivity.createDatabase();

	Thread t = new Thread(new UnicastFileAcceptConnectivity());
	t.start();
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    ConnexionView frame = new ConnexionView();
		    frame.setVisible(true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

}
