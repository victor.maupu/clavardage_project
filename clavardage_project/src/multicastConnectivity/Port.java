package multicastConnectivity;

import java.io.IOException;
import java.net.DatagramSocket;

public class Port {

 

    public static int getPortOpen2() throws Exception {
	DatagramSocket socket;
	for (int port = 4000; port <= 65535; port++) {
	    try {
		socket = new DatagramSocket(port);
		socket.close();
		return port;
	    } catch (IOException e) {
		// empty catch block
	    }
	}
	return 0;
    }
}
