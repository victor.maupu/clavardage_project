package multicastConnectivity;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class LocalAdress {
    public static InetAddress getLocalAdress() {
	Enumeration<NetworkInterface> e = null;
	try {
	    e = NetworkInterface.getNetworkInterfaces();
	} catch (SocketException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}
	while (e.hasMoreElements()) {

	    Enumeration<InetAddress> i = e.nextElement().getInetAddresses();

	    while (i.hasMoreElements()) {

		InetAddress a = i.nextElement();
		if (a.isSiteLocalAddress()) {
		    return a;
		}

	    }

	}
	return null;
    }

}
