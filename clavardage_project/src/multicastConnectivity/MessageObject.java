package multicastConnectivity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import modele.MessageSync;
import modele.User;

public class MessageObject {
    /*
     * Construit le message pour envoyer l'objet
     */

    public static byte[] CreateMessageObject(String contenu, String type) {
	// Prepare Data
	MessageSync message = new MessageSync(contenu, User.QuiSuisJe(), type);
	// String message = "COU";
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	ObjectOutputStream oos = null;
	try {
	    oos = new ObjectOutputStream(baos);
	} catch (IOException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}
	try {
	    oos.writeObject(message);
	} catch (IOException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}

	return baos.toByteArray();
    }

    public static MessageSync ReceiveMessageObject(byte[] buf) {
	// Récupération de l'objet MessageSync
	ByteArrayInputStream bais = new ByteArrayInputStream(buf);
	ObjectInputStream ois = null;
	MessageSync message = null;
	try {
	    ois = new ObjectInputStream(bais);
	} catch (IOException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}
	try {
	    Object readObject = ois.readObject();
	    if (readObject instanceof MessageSync) {
		message = (MessageSync) readObject;
	    }
	} catch (Exception e) {

	}
	return message;
    }
}
