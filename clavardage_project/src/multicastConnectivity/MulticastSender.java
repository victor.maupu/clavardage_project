package multicastConnectivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import actionListener.ConnexionViewListener;
import actionListener.MainViewListener;
import modele.MessageSync;
import modele.User;
import views.MainView;

public class MulticastSender {
    protected Socket sock;
    protected PrintWriter out;
    protected BufferedReader br;
    protected DatagramSocket socket;
    protected InetAddress group;
    protected byte[] buf;

    public void multicast(String multicastMessage, String multicastType) {
	int port = -1;
	try {
	    socket = new DatagramSocket(0);
	} catch (SocketException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	try {
	    group = InetAddress.getByName("230.0.0.0");
	} catch (UnknownHostException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	buf = MessageObject.CreateMessageObject(multicastMessage, multicastType);
	boolean erreurpseudo = false;
	DatagramPacket packet = new DatagramPacket(buf, buf.length, group, 4449);
	if (multicastType == "[IMHERE]") {
	    try {
		socket.send(packet);
		port = socket.getLocalPort();
		socket.close();
		DatagramSocket socket = new DatagramSocket(port);
		byte buffer[] = new byte[1024];
		long temps_depart = System.currentTimeMillis();
		long duree = 2000; // en millisecondes
		while ((temps_depart - System.currentTimeMillis()) < duree) {
		    DatagramPacket data = new DatagramPacket(buffer, buffer.length);
		    socket.setSoTimeout(1000);
		    socket.receive(data);
		    MessageSync message = MessageObject.ReceiveMessageObject(buffer);
		    if (message.getType().equals("[ERREURPSEUDO]")) {
			socket.close();
			erreurpseudo = true;
			User.listeUsers.remove(0);
			ConnexionViewListener.cv.setVisible(true);
			ConnexionViewListener.cv.pseudo_false.setVisible(true);
			break;
		    } else if (message.getType().equals("[METOOIMHERE]")) {
			new User(message.getContenu(), data.getAddress().getHostAddress());
		    }
		}

	    } catch (SocketTimeoutException e) {
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    if (!erreurpseudo) {
		MainView window = new MainView();
		window.setVisible(true);
	    }
	} else if (multicastType == "[ILEAVE]") {
	    try {
		socket.send(packet);
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	} else if (multicastType == "[ICHANGEPSEUDO]") {
	    try {
		socket.send(packet);
		port = socket.getLocalPort();
		socket.close();
		DatagramSocket socket = new DatagramSocket(port);
		byte buffer[] = new byte[1024];
		long temps_depart = System.currentTimeMillis();
		long duree = 2000; // en millisecondes
		while ((temps_depart - System.currentTimeMillis()) < duree) {
		    DatagramPacket data = new DatagramPacket(buffer, buffer.length);
		    socket.setSoTimeout(1000);
		    socket.receive(data);
		    MessageSync message = MessageObject.ReceiveMessageObject(buffer);
		    if (message.getType().equals("[ERREURCHGPSEUDO]")) {
			socket.close();
			erreurpseudo = true;
			break;
		    }
		}

	    } catch (SocketTimeoutException e) {
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    if (erreurpseudo) {
		String lastpseudo = MainViewListener.getLastPseudo();
		MainViewListener.mv.setName(lastpseudo);
		User.changeMyName(lastpseudo);
		MainViewListener.mv.setVisiblePseudoDjPris(true);
	    }
	}
    }

}
