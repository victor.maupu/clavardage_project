package multicastConnectivity;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

public class ServerRequestWhoishere extends Thread {
    private static boolean run = true;
    public static int counterreur = 0;

    public void run() {
	Timer t = new Timer();
	t.schedule(new TimerTask() {
	    @Override
	    public void run() {
		if (ServerRequestWhoishere.run) {
		    MulticastServerRequest t = new MulticastServerRequest();
		    try {
			t.request("", "WHOISHERE");
		    } catch (JSONException e1) {
		    }
		} else {
		    t.cancel();
		}

	    }
	}, 0, 2000);
    }

    public static void stopRequest() {
	if (ServerRequestWhoishere.counterreur == 2) {
	    ServerRequestWhoishere.run = false;
	    // MainView.setVisiblelblServeurDconnect(true);
	}
    }

    public static void stopRequestNow() {
	ServerRequestWhoishere.run = false;
    }
}
