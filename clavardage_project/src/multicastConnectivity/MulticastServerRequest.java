package multicastConnectivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import actionListener.ConnexionViewListener;
import actionListener.MainViewListener;
import modele.Mode;
import modele.User;
import views.MainView;

public class MulticastServerRequest extends Thread {
    protected Socket sock;
    protected PrintWriter out;
    protected BufferedReader br;
    protected DatagramSocket socket;
    protected InetAddress group;
    protected byte[] buf;

    public void request(String requestMessage, String requestType) throws JSONException {
	String retourJSON = null;
	if (requestType == "IMHERE") {
	    try {
		retourJSON = get("http://" + Mode.getIpServeur() + ":8080/ClavardageServlet/ServletClarvardage?type="
			+ requestType + "&pseudo=" + User.QuiSuisJe().getPseudo());
		if (retourJSON.equals("[ERREURPSEUDO]")) {
		    User.listeUsers.remove(0);
		    ConnexionViewListener.cv.setVisible(true);
		    ConnexionViewListener.cv.pseudo_false.setVisible(true);
		    // break;
		} else {
		    JSONObject jsonObject = new JSONObject(retourJSON);
		    JSONArray c = jsonObject.getJSONArray("Users");
		    for (int i = 0; i < c.length(); i++) {
			JSONObject obj = c.getJSONObject(i);
			String pseudo = obj.getString("pseudo");
			String ip = obj.getString("ip");
			if (User.PseudoIsInListUser(pseudo) == null) {
			    new User(pseudo, ip);
			}
		    }
		    MainView window = new MainView();
		    window.setVisible(true);
		    ServerRequestWhoishere t = new ServerRequestWhoishere();
		    t.start();
		}
	    } catch (IOException e) {
		JFrame frame = new JFrame();
		User.listeUsers.remove(0);
		ConnexionViewListener.cv.setVisible(true);
		JOptionPane.showMessageDialog(frame,
			"Serveur distant non joignable. \nSi le problème persiste, contactez votre administrateur réseaux.");
	    }

	} else if (requestType == "WHOISHERE") {
	    try {
		retourJSON = get("http://" + Mode.getIpServeur() + ":8080/ClavardageServlet/ServletClarvardage?type="
			+ requestType + "&pseudo=" + User.QuiSuisJe().getPseudo());
		JSONObject jsonObject = new JSONObject(retourJSON);
		JSONArray c = jsonObject.getJSONArray("Users");
		ArrayList<String> listeUsersPresent = new ArrayList<String>();
		for (int i = 0; i < c.length(); i++) {
		    JSONObject obj = c.getJSONObject(i);
		    String pseudo = obj.getString("pseudo");
		    String ip = obj.getString("ip");
		    listeUsersPresent.add(pseudo);
		    if (User.PseudoIsInListUser(pseudo) == null) {
			new User(pseudo, ip);
		    }
		}
		ServerRequestWhoishere.counterreur = 0;
		User.actualiseList(listeUsersPresent);
		MainView.actualiseList();
	    } catch (IOException e) {
		ServerRequestWhoishere.counterreur++;
		ServerRequestWhoishere.stopRequest();
	    }

	} else if (requestType == "ILEAVE") {
	    try {
		retourJSON = get("http://" + Mode.getIpServeur() + ":8080/ClavardageServlet/ServletClarvardage?type="
			+ requestType);
	    } catch (IOException e) {
	    }
	} else if (requestType == "ICHANGEPSEUDO") {
	    try {
		retourJSON = get("http://" + Mode.getIpServeur() + ":8080/ClavardageServlet/ServletClarvardage?type="
			+ requestType + "&pseudo=" + User.QuiSuisJe().getPseudo());
		if (retourJSON.equals("[ERREURCHGPSEUDO]")) {
		    String lastpseudo = MainViewListener.getLastPseudo();
		    MainViewListener.mv.setName(lastpseudo);
		    User.changeMyName(lastpseudo);
		    MainViewListener.mv.setVisiblePseudoDjPris(true);
		}
	    } catch (IOException e) {
		JFrame frame = new JFrame();
		String lastpseudo = MainViewListener.getLastPseudo();
		MainViewListener.mv.setName(lastpseudo);
		User.changeMyName(lastpseudo);
		JOptionPane.showMessageDialog(frame,
			"Serveur distant non joignable.\nVeuillez vous reconnecter pour accéder au service. \nSi le problème persiste, contactez votre administrateur réseaux.");
	    }

	}

    }

    public static String get(String url) throws IOException {

	String source = "";
	URL urls = new URL(url.replaceAll(" ", "%20"));
	URLConnection urlsc = urls.openConnection();
	BufferedReader in = new BufferedReader(new InputStreamReader(urlsc.getInputStream()));
	String inputLine;

	while ((inputLine = in.readLine()) != null)
	    source += inputLine;
	in.close();
	return source;
    }

}
