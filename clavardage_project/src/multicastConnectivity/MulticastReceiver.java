package multicastConnectivity;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketAddress;
import java.net.SocketException;

import modele.MessageSync;
import modele.User;
import views.MainView;

public class MulticastReceiver extends Thread {
    static MulticastSocket socket = null;
    static byte[] buf = new byte[1024];

    public void run() {
	InetAddress group;
	try {
	    socket = new MulticastSocket(4449);
	    group = InetAddress.getByName("230.0.0.0");
	    socket.joinGroup(group);
	} catch (IOException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}

	while (true) {
	    DatagramPacket packet = new DatagramPacket(buf, buf.length);
	    try {
		socket.receive(packet);
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    // String received = new String(packet.getData(), packet.getOffset(),
	    // packet.getLength());
	    packet.setLength(buf.length);
	    InetAddress myadress = LocalAdress.getLocalAdress();
	    if (!myadress.equals(packet.getAddress())) {

		// Récupération de l'objet MessageSync
		MessageSync message = MessageObject.ReceiveMessageObject(buf);

		if (message.getType().equals("[IMHERE]")) {
		    User userEmmetteur = message.getEmmeteur();
		    String ipUser = User.PseudoIsInListUser(userEmmetteur.getPseudo());
		    if (ipUser == null) {
			SocketAddress serveur = packet.getSocketAddress();
			byte[] buf = MessageObject.CreateMessageObject(User.QuiSuisJe().getPseudo(), "[METOOIMHERE]");
			DatagramPacket dataSent = new DatagramPacket(buf, buf.length, serveur);
			DatagramSocket socket = null;
			try {
			    socket = new DatagramSocket();
			} catch (SocketException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
			try {
			    socket.send(dataSent);
			} catch (IOException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
			User u = User.getUserByIP(packet.getAddress().getHostAddress());
			if (u != null) {
			    u.setPseudo(userEmmetteur.getPseudo());

			} else {
			    u = new User(userEmmetteur.getPseudo(), packet.getAddress().getHostAddress());
			}
			MainView.addUserList(u);
		    } else {
			if (ipUser.equals(packet.getAddress().getHostAddress())) {
			} else {
			    SocketAddress serveur = packet.getSocketAddress();
			    byte[] buf = MessageObject.CreateMessageObject("", "[ERREURPSEUDO]");
			    DatagramPacket dataSent = new DatagramPacket(buf, buf.length, serveur);
			    DatagramSocket socket = null;
			    try {
				socket = new DatagramSocket();
			    } catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			    }
			    try {
				socket.send(dataSent);
			    } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			    }

			}
		    }
		} else if (message.getType().equals("[ILEAVE]")) {
		    MainView.deleteUserList(message.getEmmeteur());
		} else if (message.getType().equals("[ICHANGEPSEUDO]")) {
		    User userEmmetteur = message.getEmmeteur();
		    String ipUser = User.PseudoIsInListUser(userEmmetteur.getPseudo());
		    if (ipUser == null) {
			MainView.deleteUserList(User.getUserByIP(userEmmetteur.getAdresseIP()));
			MainView.addUserList(userEmmetteur);
			User.listeUsers.remove(User.getUserByIP(userEmmetteur.getAdresseIP()));
			User.listeUsers.add(userEmmetteur);
		    } else {
			SocketAddress serveur = packet.getSocketAddress();
			byte[] buf = MessageObject.CreateMessageObject("", "[ERREURCHGPSEUDO]");
			DatagramPacket dataSent = new DatagramPacket(buf, buf.length, serveur);
			DatagramSocket socket = null;
			try {
			    socket = new DatagramSocket();
			} catch (SocketException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
			try {
			    socket.send(dataSent);
			} catch (IOException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		    }
		}
	    }
	}

    }

    public static void close() {
	socket.close();
    }
}
