package unicastConnectivity;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.ConversationController;
import databaseConnectivity.DatabaseConnectivity;
import modele.Message;
import modele.User;

public class UnicastConnectivity {

    private User user;
    private ObjectOutputStream out;
    private Socket sock;
    private int port;
    public ConversationController cc;

    public UnicastConnectivity(User u, ConversationController c) {
	this.user = u;
	this.port = 7002;
	this.cc = c;
	this.initConversation();
    }

    public UnicastConnectivity(Socket sock2, ConversationController c, User u2) throws InterruptedException {
	this.sock = sock2;
	this.cc = c;
	this.user = u2;

	try {
	    Thread t1 = new Thread(new UnicastReceiver(this));
	    t1.start();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public void initConversation() {
	Thread t1;
	try {
	    this.sock = new Socket(user.getAdresseIP(), port);
	    t1 = new Thread(new UnicastReceiver(this));
	    t1.start();
	} catch (UnknownHostException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (ConnectException e) {
	    JFrame frame = new JFrame();
	    JOptionPane.showMessageDialog(frame, "Le correspondant n'est pas joignable");

	} catch (IOException e) {
	    e.printStackTrace();
	}

    }

    public Socket getSocket() {
	return this.sock;
    }

    public void sendmsg(Message m) {
	try {
	    this.out = new ObjectOutputStream(this.sock.getOutputStream());
	    out.writeObject(m);
	    DatabaseConnectivity.addMessage(m);
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public void receiveMessage(Message m) {
	this.cc.receiveMessage(m);
    }

    public void closeConversation() {
	this.cc.closeConversation();

    }

    public User getUser() {
	return this.user;
    }

    public void closeSocket() {
	try {
	    this.sock.close();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public void setSocket(Socket sock2) {
	this.sock = sock2;

    }

}
