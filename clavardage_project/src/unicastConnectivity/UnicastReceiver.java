package unicastConnectivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import databaseConnectivity.DatabaseConnectivity;
import modele.Message;
import modele.User;

public class UnicastReceiver implements Runnable {

    private UnicastConnectivity uc;
    BufferedReader br;
    ObjectInputStream in;

    public UnicastReceiver(UnicastConnectivity uc) throws IOException {
	this.uc = uc;
    }

    @Override
    public void run() {
	boolean cont = true;
	InputStream is = null;
	while (cont) {
	    try {
		is = uc.getSocket().getInputStream();
		in = new ObjectInputStream(is);
		Message m = (Message) in.readObject();
		m.setEmetteur(uc.getUser());
		m.setRecepteur(User.QuiSuisJe());
		DatabaseConnectivity.addMessage(m);
		this.uc.receiveMessage(m);
	    } catch (IOException e) {

		this.uc.closeConversation();
		cont = false;
	    } catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

}
