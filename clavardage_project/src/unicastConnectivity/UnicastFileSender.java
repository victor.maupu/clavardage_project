package unicastConnectivity;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import modele.User;

public class UnicastFileSender implements Runnable {

    private User u;
    private File file;

    public UnicastFileSender(User u, File file) {
	this.u = u;
	this.file = file;
    }

    public void run() {
	FileInputStream fis = null;
	Socket sock = null;
	try {
	    sock = new Socket(u.getAdresseIP(), 8000);
	    PrintWriter out = new PrintWriter(sock.getOutputStream());
	    out.println(file.getName());
	    out.flush();
	    DataOutputStream dos = new DataOutputStream(sock.getOutputStream());
	    fis = new FileInputStream(file);
	    byte[] buffer = new byte[4096];

	    while (fis.read(buffer) > 0) {
		dos.write(buffer);
	    }
	    fis.close();
	    dos.close();
	    sock.close();

	} catch (IOException e) {
	    JFrame frame = new JFrame();
	    JOptionPane.showMessageDialog(frame, "Une erreur est survenue pendant l'envoi du fichier.");
	    frame = null;
	} finally {
	    if (sock != null)
		try {
		    sock.close();
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	}
	JFrame frame = new JFrame();
	JOptionPane.showMessageDialog(frame, "Le fichier a bien été envoyé !");
	frame = null;
    }
}
