package unicastConnectivity;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class UnicastFileAcceptConnectivity implements Runnable {

    private ServerSocket serverSocket;
    private Socket sock;
    private int port;

    public UnicastFileAcceptConnectivity() {
	this.port = 8000;
    }

    @Override
    public void run() {
	try {
	    this.serverSocket = new ServerSocket(this.port);
	} catch (IOException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}
	while (true) {
	    try {

		this.sock = null;
		this.sock = this.serverSocket.accept();

		Thread t = new Thread(new UnicastFileReceiver(this.sock));
		t.start();
	    } catch (BindException be) {
		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame, "Il semble qu'une instance soit déjà en cours d'execution sur "
			+ "cette machine. \nSi le problème persiste, contactez votre administrateur réseaux.");
		break;
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	}
    }
}
