package unicastConnectivity;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import modele.User;

public class UnicastFileReceiver implements Runnable {

    private Socket sock;
    BufferedReader br;
    ObjectInputStream in;

    public UnicastFileReceiver(Socket sock) throws IOException {
	this.sock = sock;
    }

    @Override
    public void run() {
	boolean cont = true;
	FileOutputStream fos = null;
	JFrame frame = new JFrame();
	while (cont) {
	    try {
		BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
		String name = in.readLine();
		User u = User.getUserByIP(sock.getInetAddress().getHostAddress());
		DataInputStream dis = new DataInputStream(sock.getInputStream());
		SimpleDateFormat h = new SimpleDateFormat("dd_MM_yyyy_hh_mm");
		JOptionPane.showMessageDialog(frame, "Vous avez reçu un fichier de " + u.getPseudo()
			+ "\nVeuillez choisir un dossier pour l'enregistrer.");
		Date currentTime_1 = new Date();
		String heure = h.format(currentTime_1);
		String nomFic = "/" + heure + "_" + u.getPseudo() + "_" + name;
		try {
		    JFileChooser choix = new JFileChooser();
		    choix.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		    choix.showOpenDialog(null);
		    fos = new FileOutputStream(choix.getSelectedFile().getPath() + nomFic);
		    byte[] buffer = new byte[4096];

		    int filesize = 160000000; // Send file size in separate msg
		    int read = 0;
		    int remaining = filesize;
		    while ((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
			remaining -= read;
			fos.write(buffer, 0, read);
		    }

		    fos.close();
		    dis.close();
		    JOptionPane.showMessageDialog(frame,
			    "Le fichier a bien été enregistré dans : \n" + choix.getSelectedFile().getPath() + nomFic);
		    frame = null;
		} catch (Exception e) {
		    JOptionPane.showMessageDialog(frame, "Le transfert n'a pas eu lieu.");
		}
	    } catch (IOException e) {

	    }
	}
    }
}
