package controller;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

import databaseConnectivity.DatabaseConnectivity;
import modele.Conversation;
import modele.Message;
import modele.User;
import views.ConversationView;

public class ConversationController implements Runnable {

    public Conversation c;
    public static ArrayList<ConversationController> list_cv = new ArrayList<ConversationController>();
    ConversationView cv;

    public ConversationController(User u) {
	c = new Conversation(u, this);
	this.runConv();
	ConversationController.list_cv.add(this);
    }

    public ConversationController(Socket sock) {
	c = new Conversation(sock, this, User.getUserByIP(sock.getInetAddress().getHostAddress()));
	this.runConv();
    }

    public void runConv() {

	cv = new ConversationView(this);
	ArrayList<Message> m = DatabaseConnectivity.getOrderedConversation(User.QuiSuisJe(), c.getUser2());
	for (Message mess : m) {
	    if (mess.getEmetteur().equals(User.QuiSuisJe())) {
		this.cv.addMessageEnvoye(mess);
	    } else {
		this.cv.addMessageRecu(mess);
	    }
	}
    }

    public void sendMessage(String s) {
	Message m = new Message(new Date(), s, User.QuiSuisJe(), c.getUser2());
	this.c.addMessage(m);
	this.cv.addMessageEnvoye(m);
	this.c.getUnicastConnectivity().sendmsg(m);
	this.cv.textPane.setText("");
    }

    @Override
    public void run() {
	this.runConv();
    }

    public synchronized void receiveMessage(Message m) {
	this.c.receiveMessage(m);
	this.cv.addMessageRecu(m);
    }

    public void closeConversation() {
	this.cv.close();
    }

    public void closeSocket() {
	this.c.getUnicastConnectivity().closeSocket();
    }

    public ArrayList<User> getUsers() {
	return this.c.getUsers();
    }

    public static ConversationController findConvController(String ip) {
	for (ConversationController cv : ConversationController.list_cv) {
	    if (cv.c.getUser2().getAdresseIP().equals(ip)) {
		return cv;
	    }
	}
	return null;
    }

    public void reactivate(Socket sock) {
	this.cv.reactivate();
	this.c.getUnicastConnectivity().setSocket(sock);

    }

}
